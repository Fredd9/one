<?php
/* @var $this PageController */

$this->breadcrumbs = array(
    'Категория' . $model->category->title,
);

?>
    <h1><?php echo $model->title; ?></h1>
<?php
echo date("j.m.Y H:i", $model->data);
?>
    <hr/>

    </br>

<?php echo $model->content; ?>

    <hr/>
<?php if (!empty(Yii::app()->user->id)) { ?>

    <?php if (Yii::app()->user->hasFlash('contact')): ?>

        <div class="flash-success">

            <?php echo Yii::app()->user->getFlash('contact'); ?>

        </div>

    <?php else: ?>

        <?php
        echo $this->renderPartial('newComent', array('model' => $newComent));
        ?>

    <?php endif ?>

<?php
} else {
    echo '<h4>Комментарии:</h4> <br> Для того что бы оставить комментарии Вам необходимо зарегестрироваться';
}
?>




<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => Coment::all($model->id),
    'itemView' => '_viewComent',
)); ?>