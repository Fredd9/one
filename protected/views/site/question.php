<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'question-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Поля с <span class="required">*</span> обязательны.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'category'); ?>
        <?php echo $form->dropDownList($model, 'category', Category::all()); ?>
        <?php echo $form->error($model, 'category'); ?>
    </div>

    <?php if (Yii::app()->user->isGuest) { ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'user_email'); ?>
            <?php echo $form->textField($model, 'user_email'); ?>
            <?php echo $form->error($model, 'user_email'); ?>
        </div>
    <? } else { ?>
        <div>Email:</div>
        <div><?= $email ?></div>
    <?php } ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'content'); ?>
        <?php echo $form->textArea($model, 'content', array('rows' => 6, 'cols' => 60)); ?>
        <?php echo $form->error($model, 'content'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'Введи код'); ?>
        <div>
            <?php $this->widget('CCaptcha'); ?>
            <?php echo $form->textField($model, 'verifyCode'); ?>
        </div>
        <div class="hint">Жми на кнопку если заполнил.
        </div>
        <?php echo $form->error($model, 'Введи код'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Отправить'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div>