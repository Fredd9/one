<?php
//asdfadfasdfasdsadasdf
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

return array(
    'theme' => 'bootstrap',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My New CMS',
    'language' => 'ru',

    // preloading 'log' component
    'preload' => array('log', 'bootstrap',),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),

    'modules' => array(
        // uncomment the following to enable the Gii tool
        'admin',

        'gii' => array(
            'generatorPaths' => array(
                'bootstrap.gii',
            ),
            'class' => 'system.gii.GiiModule',
            'password' => '123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),

    ),
//  'components' => array(
//    'mail' => array(
//    'class' => 'ext.mail.YiiMail',
//    'transportType' => 'smtp', //smtp, php
//    'transportOptions' => array(
//        'host' => 'smtp.gmail.com',
//        'port' => '465',
//        'username' => 'testcaseag@gmail.com',
//        'password' => 'room1007',
//        'encryption'=>'ssl',
//    ),
//    'viewPath' => 'application.views.emails',
//    'logging' => true,
//    'dryRun' => false
//),
    // application components
    'components' => array(
        'mail' => array(
            'class' => 'ext.mail.YiiMail',
            'viewPath' => 'application.views.emails',
            'logging' => true,
            'testEmail' => 'testcaseag@gmail.com',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'testcaseag@gmail.com',
                'password' => 'room1007',
                'port' => '587',
                'encryption' => 'tls',
            ),
        ),
        'authManager' => array(
            // ����� ������������ ���� �������� �����������
            'class' => 'PhpAuthManager',
            // ���� �� ���������. ���, ��� �� ������ � ����� � �����.
            'defaultRoles' => array('guest'),
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'user' => array(
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

            ),
            'showScriptName' => false,
        ),
        /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
        // uncomment the following to use a MySQL database

        'db' => array(

            'connectionString' => 'mysql:host=localhost;dbname=cms',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '123456',
            'charset' => 'utf8',
            'tablePrefix' => 'cms_',

        ),

        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'testcaseag1@gmail.com',
    ),
);