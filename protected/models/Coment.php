<?php

/**
 * This is the model class for table "{{coment}}".
 *
 * The followings are the available columns in table '{{coment}}':
 * @property integer $id
 * @property string $content
 * @property integer $user_id
 * @property integer $page_id
 * @property string $data
 */
class Coment extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{coment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content', 'required'),
            array('user_id, page_id', 'numerical', 'integerOnly' => true),
            array('content', 'length', 'max' => 255),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, content, user_id, page_id, data', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'page' => array(self::BELONGS_TO, 'Page', 'page_id'),

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'content' => 'Содержание',
            'user_id' => 'Пользователь',
            'page_id' => 'Страница',
            'data' => 'Дата создания',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('page_id', $this->page_id);
        $criteria->compare('data', $this->data, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function beforeSave()
    {

        if ($this->isNewRecord)
            $this->data = time();

        return parent::beforeSave();
    }


    public static function all($page_id)
    {
        $criteria = new CDbCriteria;
        $criteria->order = ('data DESC');

        $criteria->compare('page_id', $page_id);

        return new CActiveDataProvider('Coment', array(
            'criteria' => $criteria));

    }

}