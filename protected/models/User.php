<?php

class UserRole
{
    const GUEST = 0;
    const USER = 1;
    const ADMIN = 2;

    public static function items()
    {
        return array(
            self::GUEST => 'Guest',
            self::USER => 'User',
            self::ADMIN => 'Admin',
        );
    }

    public static function getItemTitle($itemNum)
    {
        $items = self::items();
        return $items[$itemNum];
    }

    public static function isValid($role)
    {
        return array_key_exists($role, self::items());
    }
}

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property integer $role
 * @property string $data
 * @property integer $ban
 */
class User extends CActiveRecord
{

    public $verifyCode;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email, username, password', 'required'),
            array('email', 'email'),
            array('role, ban', 'numerical', 'integerOnly' => true),
            array('email, username, password', 'length', 'max' => 255),

            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => 'registration'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, email, username, password, role, data, ban', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'email' => 'E-mail',
            'username' => 'Имя',
            'password' => 'Пароль',
            'role' => 'Статус',
            'data' => 'Дата Регистрации',
            'ban' => 'Бан',
            'verifyCode' => 'Код с картинки',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('role', $this->role);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('ban', $this->ban);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {

        if ($this->isNewRecord)
            $this->data = time();
        $this->role = 1;
        $this->ban = 0;

        $this->password = md5('ab23dee61' . $this->password);
        return parent::beforeSave();

    }

    public static function all()
    {

        return CHtml::listData(self::model()->findAll(), 'id', 'username'); //Использовали хелпер для уменьшения обьёма кода

    }
}
