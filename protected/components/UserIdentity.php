<?php

class UserIdentity extends CUserIdentity {
  
    // Будем хранить id.
    protected $_id;
    protected $_email;
 
 
    // Данный метод вызывается один раз при аутентификации пользователя.
    public function authenticate(){
        // Производим стандартную аутентификацию, описанную в руководстве.
        $user = User::model()->find('LOWER(email)=?', array(strtolower($this->username)));
        if($user->ban == 1) die ('Ваш аккаунт забанен, если вы зарегистрировались первый раз ждите активации!!!');
            
            
        
        if(($user===null) || (md5('ab23dee61'.$this->password))!==$user->password) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            // В качестве идентификатора будем использовать id, а не username,
            // как это определено по умолчанию. Обязательно нужно переопределить
            // метод getId(см. ниже).
            $this->_id = $user->id;
            $this->_email = $user->email;
 
            // Далее логин нам не понадобится, зато имя может пригодится
            // в самом приложении. Используется как Yii::app()->user->name.
            // realName есть в нашей модели. У вас это может быть name, firstName
            // или что-либо ещё.
            $this->username = $user->username;
 
            $this->errorCode = self::ERROR_NONE;
        }
       return !$this->errorCode;
    }
 
    public function getId(){
        return $this->_id;
    }

    public function getEmail()
    {
        return $this->_email;
    }
}