<?php
	class WebUser extends CWebUser {
  
  
    private $_model = null;

   public $_email;
 
 
    function getRole() {
        if($user = $this->getModel()){
            return $user->role;
        }
    }
 
    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }

    /**
     * Returns a value that uniquely represents the user.
     * @return mixed the unique identifier for the user. If null, it means the user is a guest.
     */
    public function getEmail()
    {
        return $this->getState('__email');
    }
}
