<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Создать пользователя', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

    <h1>Журнал пользователей</h1>


<?php echo CHtml::link('Расширеный поиск', '#', array('class' => 'search-button')); ?>
    <div class="search-form" style="display:none">
        <?php $this->renderPartial('_search', array(
            'model' => $model,
        )); ?>
    </div><!-- search-form -->

<?php
echo '<br />';
echo '<br />';
echo '<br />';
echo CHtml::form();
echo CHtml::submitButton('Admin', array('name' => 'admin'));
echo CHtml::submitButton('NoAdmin', array('name' => 'noadmin'));
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'user-grid',
    'selectableRows' => 2,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        array('class' => 'CCheckBoxColumn',
            'id' => 'user_id',
        ),
        'email',
        'username',
        'password',
        'role' => array(
            'name' => 'role',
            'value' => '($data->role==1)?"User":"Admin"',
            'filter' => array(2 => "Admin", 1 => "User"),
        ),
        'data' => array(
            'name' => 'data',
            'value' => 'date("j.m.Y H.i",$data->data)',
            'filter' => false,
        ),
        'ban' => array(
            'name' => 'ban',
            'value' => '($data->ban==1)?"Бан":" "',
            'filter' => array(2 => "Нет", 1 => "Да"),
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
<?php
echo CHtml::endform();
?>