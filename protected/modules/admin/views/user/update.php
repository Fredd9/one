<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Журнал пользователей', 'url' => array('index')),
    array('label' => 'Создать пользователя', 'url' => array('create')),
    array('label' => 'Просмотреть пользователей', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Изменить пароль', 'url' => array('password', 'id' => $model->id)),
);
?>

    <h1>Изменить пользователя <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>