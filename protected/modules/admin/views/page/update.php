<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs = array(
    'Pages' => array('index'),
    $model->title => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Журнал статей', 'url' => array('index')),
    array('label' => 'Создать статью', 'url' => array('create')),
    array('label' => 'Просмотр Статейы', 'url' => array('view', 'id' => $model->id)),
);
?>

    <h1>Изменить Статью <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>