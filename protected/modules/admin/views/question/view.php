<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs = array(
    'Questions' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Журнал заявок', 'url' => array('index')),
    array('label' => 'Удалить заявку', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Ответить на заявку', 'url' => array('/admin/answer/update', 'question_id' => $model->id)),
);
?>

<h1>Заявка #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'category' => array(
            'name' => 'category',
            'value' => function ($row) {
                    return $row->categoryInstance->title;
                },
            'type' => 'raw',
        ),
        'content',
        'user_id' => array(
            'name' => 'user_id',
            'value' => function ($row) {
                    return (!empty($row->user->username) ? $row->user->username : 'Guest');
                },
            'type' => 'raw',
        ),
        'status' => array(
            'name' => 'status',
            'value' => function ($row) {
                    return ($row->status) ? 'Отвечено' : 'В процессе';
                },
            'type' => 'raw',
        ),
        'data' => array(
            'name' => 'data',
            'value' => function ($row) {
                    return date("j.m.Y H:i", $row->data);
                },
            'type' => 'raw',
        ),
    ),
)); ?>
