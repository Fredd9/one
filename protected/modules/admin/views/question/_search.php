<?php
/* @var $this QuestionController */
/* @var $model Question */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'category'); ?>
        <?php echo $form->dropDownList($model, 'category', Category::all(), array('empty' => '')); ?>
    </div>


    <div class="row">
        <?php echo $form->label($model, 'user_id'); ?>
        <?php echo $form->dropDownList($model, 'user_id', User::all(), array('empty' => '')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array('0' => 'В очереди', '1' => 'Отвечено')); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Поиск'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->