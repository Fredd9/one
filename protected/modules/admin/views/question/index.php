<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs = array(
    'Questions' => array('index'),
    'Manage',
);


Yii::app()->clientScript->registerScript('search', <<<JS
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#question-grid').yiiGridView('update', {
       data: $(this).serialize()
    });
    return false;
});
JS
);
?>

<h1>Журнал Заявок</h1>



<?php echo CHtml::link('Расширеный поиск', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'question-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'category' => array(
            'name' => 'category',
            'value' => function ($row) {
                    return $row->categoryInstance->title;
                },
            'filter' => Category::all(),
            'type' => 'raw',
        ),
        'content',
        'user_id' => array(
            'name' => 'user_id',
            'value' => function ($row) {
                    return (!empty($row->user->uswername) ? $row->user->username : 'Guest');
                },
            'filter' => User::all(),
            'type' => 'raw',
        ),
        'user_email' => array(
            'name' => 'user_email',
            'value' => function ($row) {
                    return (!empty($row->user->email) ? $row->user->email : $row->user_email);
                },
            'filter' => User::all(),
            'type' => 'raw',
        ),
        'status' => array(
            'name' => 'status',
            'value' => function ($row) {
                    return ($row->status) ? 'Отвечено' : 'В процессе';
                },
            'filter' => array([1 => 'Отвечено', 0 => 'В процессе']),
            'type' => 'raw',
        ),
        'data' => array(
            'name' => 'data',
            'value' => function ($row) {
                    return date("j.m.Y H:i", $row->data);
                },
            'filter' => false,
            'type' => 'raw',
        ),
        array(
            'class' => 'CButtonColumn',
            'updateButtonOptions' => array('style' => 'display:none'),
        ),
    ),
)); ?>
