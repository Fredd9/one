<?php
/* @var $this ComentController */
/* @var $model Coment */

$this->breadcrumbs = array(
    'Coments' => array('index'),
    'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#coment-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Журнал Комментариев</h1>


<?php echo CHtml::link('Рвсширеный поиск', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'coment-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'content',
        'user_id' => array(
            'name' => 'user_id',
            'value' => '$data->user->username',
            'filter' => User::all(),
        ),
        'page_id' => array(
            'name' => 'page_id',
            'value' => '$data->page->title',
            'filter' => Page::all(),
        ),
        'data' => array(
            'name' => 'data',
            'value' => 'date("j.m.Y H.i",$data->data)',
            'filter' => false,
        ),
        array(
            'class' => 'CButtonColumn',
            'updateButtonOptions' => array('style' => 'display:none')
        ),
    ),
)); ?>

