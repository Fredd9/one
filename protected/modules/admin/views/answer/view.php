<?php
/* @var $this AnswerController */
/* @var $model Answer */

$this->breadcrumbs = array(
    'Answers' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Журнал ответов', 'url' => array('index')),
    array('label' => 'Update Answer', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Удалить ответ', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),

);
?>

<h1>View Answer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'content',
        'question_id',
        'data',
        'user_email',
    ),
)); ?>
