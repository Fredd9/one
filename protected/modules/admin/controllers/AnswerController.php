<?php

Yii::import('ext.mail.YiiMailMessage');

class AnswerController extends Controller
{


    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'index', 'update', 'delete'),
                'roles' => array('2'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Answer;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Answer'])) {
            $model->attributes = $_POST['Answer'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($question_id)
    {
        $model = new Answer;

        $question = Question::model()->findByPk($question_id);

        $model->question_id = $question->id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Answer'])) {
            $model->attributes = $_POST['Answer'];
            $model->user_email = $question->getUserEmail();
            if ($model->save()) {
                if ($this->sendMessage($model)) {
                    Yii::app()->user->setFlash('success', 'Сообщение успешно отправленно');
                    Question::model()->updateByPk($question_id, array('status' => 1));
                } else {
                    Yii::app()->user->setFlash('error', 'При отправленни произошла ошибка');
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'question' => $question,
        ));
    }

    /**
     *
     * @param Answer $model
     * @return bool
     */
    protected function sendMessage(Answer $model)
    {
        $message = new YiiMailMessage();
        /** @var YiiMail $maler */
        $maler = Yii::app()->mail;
//        $tmp = Yii::app()->params['adminEMail']
        $message->setFrom(Yii::app()->params['adminEmail']);
        $message->setSubject($model->subject);
        $message->setBody($model->content);
        $message->setTo($model->user_email);

        return $maler->send($message);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new Answer('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Answer']))
            $model->attributes = $_GET['Answer'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Answer the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Answer::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Answer $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'answer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
