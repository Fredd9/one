<?php

class PageController extends Controller
{


    public function actionIndex($id)
    {
        $models = Page::model()->findAllByAttributes(array('category_id' => $id));
        $this->render('index', array('models' => $models));
    }

    public function actionView($id)
    {
        $model = Page::model()->findByPk($id);

        $newComent = new Coment;


        if (isset($_POST['Coment'])) {
            $newComent->attributes = $_POST['Coment'];

            if (empty($user_id)) {
                $newComent->user_id = Yii::app()->user->id;
            } else {
                echo Yii::app()->user->setFlash('?????????? ??????????????????');
            }


            $newComent->page_id = $id;

            $newComent->save();
            $this->refresh();
        }


        $this->render('view', array('model' => $model, 'newComent' => $newComent));
    }
}